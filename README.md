**Flask-based Gaze Detection Application**

This application uses Flask, OpenCV, and dlib to perform real-time facial landmark detection to determine if a person is focused or not.

**Features**

Real-time video capture using OpenCV.
Face detection using dlib's frontal face detector.
Facial landmark detection using a pre-trained shape predictor model.
Displays detected faces with rectangles and facial landmarks with circles on the video feed.
Technologies Used
Python: The primary programming language used for development.
Flask: A lightweight WSGI web application framework for Python.
OpenCV: An open-source computer vision and machine learning software library.
dlib: A toolkit for making real-world machine learning and data analysis applications in C++ and Python.

**Prerequisites**
Python 3.x
Flask
OpenCV
dlib
A pre-trained shape predictor model (e.g., shape_predictor_68_face_landmarks.dat)